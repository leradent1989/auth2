
INSERT INTO public.users (entity_id,full_name,email,password,city,country,gender,work_place,about,birth_date,profile_picture,profile_background_picture,creation_date,last_modified_date) VALUES
                                                                                                                                                                                       (100,'Alex Smith','alex@gmail.com','$2a$10$BXH1wlAJPIMXvjnJTBoRuea4CvZwSs8/Zqz4bDRZBDJ6hxvXoHlqq','New York','USA','male','UIGA','successful investor',TO_DATE('25-JUl-1977', 'dd-MON-yyyy'),'https://qph.cf2.quoracdn.net/main-qimg-ed424b4d548229863a57603462976e3e.webp' ,'https://photographylife.com/wp-content/uploads/2017/01/Best-of-2016-Nasim-Mansurov-20.jpg',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')) ,
                                                                                                                                                                                       (101,'Cris Thomson','cris@gmail.com','$2a$10$BXH1wlAJPIMXvjnJTBoRuea4CvZwSs8/Zqz4bDRZBDJ6hxvXoHlqq','London','Great Britain','male','JklO','web designer',TO_DATE('25-AUG-1986', 'dd-MON-yyyy'), 'https://assets.thehansindia.com/h-upload/2020/05/16/969648-k-pop-singer-bts-v-most-han.webp','https://ichef.bbci.co.uk/news/999/cpsprodpb/6D5A/production/_119449972_10.jpg',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                                                                                                                       (102,'Roger Williams','roger@gmail.com','$2a$10$BXH1wlAJPIMXvjnJTBoRuea4CvZwSs8/Zqz4bDRZBDJ6hxvXoHlqq','LA','USA','male','FAliy Hospital','surgeon',TO_DATE('15-APR-1997', 'dd-MON-yyyy'),'https://www.thecoldwire.com/wp-content/uploads/2021/11/Closeup-portrait-of-a-handsome-man-at-gym.jpeg','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7SPMxYYTAmSxcMRvEIwePcBNpJi9eEdEM9A&usqp=CAU',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy'));






INSERT INTO public.posts (entity_id,user_id,content,post_type,parent_id,creation_date,last_modified_date) VALUES
                                                                                                       (100,102,'Hy Today is a good day Im going to visit very interesting place','post',null,TO_DATE('25-JUN-2020', 'dd-MON-yyyy'),TO_DATE('25-JUN-2017', 'dd-MON-yyyy')),
                                                                                                       (101,100,'My Scala course is finally released on the Manning learning platform. I''ve invested quite a lot of time and it was challenging due to the constantly challenging conditions in Ukraine (power outages, missile attacks)','post',null,TO_DATE('25-JUN-2017', 'dd-MON-yyyy'),TO_DATE('25-JUN-2020', 'dd-MON-yyyy')),
                                                                                                       (102,101, 'Understood - yours was a point well made. I’m only exploring the discussion. Some of my procedural coding has had way too many indents, I fully admit. FP can be both beautiful and functional!','post',null,TO_DATE('25-JUN-2021', 'dd-MON-yyyy'),TO_DATE('25-JUN-2021', 'dd-MON-yyyy')),
                                                                                                       (103,100,'However, in real codebases, you are dealing with longer nested code blocks which are not that readable anymore, even for proficient imperative programmers. Additionally, as Tomasz Lipinski noted, they can have some hidden unexpected parts.My objective here was to make a point .','post',null,TO_DATE('25-JUN-2022', 'dd-MON-yyyy'),TO_DATE('25-JUN-2022', 'dd-MON-yyyy')),
                                                                                                       (104,102,'When I started my programming journey I dreamed of a widescreen display  The indentation in my code was killing me.Later in my career, I learned that a code needs to read like a book  No unnecessary words, snappy, and to the point!Thats what list comprehensions are for.','post',null,TO_DATE('25-JUN-2021', 'dd-MON-yyyy'),TO_DATE('25-JUN-2021', 'dd-MON-yyyy')),
                                                                                                       (105,101,'I am immensely grateful to the organizers for recognizing my potential and inviting me to the renowned PyCon 2023 conference, which took place in the picturesque city of Florence, Italy.  The conference served as an invaluable platform for industry experts, thought leaders.','post',null,TO_DATE('25-JUN-2022', 'dd-MON-yyyy'),TO_DATE('25-JUN-2022', 'dd-MON-yyyy')),
                                                                                                       (106,100,'ddffgkrksmxme','comment',100,TO_DATE('25-JUN-2019', 'dd-MON-yyyy'),TO_DATE('25-JUN-2021', 'dd-MON-yyyy'));




INSERT INTO public.chats (entity_id,creation_date,last_modified_date) VALUES

                                                                   (207,TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                   (208,TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                   (209,TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                   (210,TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                   (211,TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                   (212,TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                   (213,TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy'));



INSERT INTO public.messages (entity_id,user_id,chat_id,content,creation_date,last_modified_date) VALUES

                                                                                              (200,102,207,'Hy',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                              (201,101,207,'Hello1',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                              (202,101,208,'Hy',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                              (203,102,208,'Hello2',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                              (204,100,209,'Hy',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                              (205,101,209,'Hy',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                              (206,101,207,'Hello',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy'));
INSERT INTO public.friends(entity_id,user_id,friend_id,status,creation_date,last_modified_date) VALUES

                                                                                             (214,100,101,'fulfilled',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                             (215,101,100,'fulfilled',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                             (216,102,101,'fulfilled',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                             (217,100,102,'pending',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                             (218,101,102,'pending',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                             (219,102,100,'pending',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy'));





INSERT INTO public.users_friends (id,user_id,user_friend_id) VALUES

                                                                 (81,100,215),
                                                                 (82,100,216),
                                                                 (83,101,214),
                                                                 (84,101,216),
                                                                 (85,102,214),
                                                                 (86,102,215);


INSERT INTO public.users_chats (id,user_id,chat_id) VALUES

                                                                 (91,100,207),
                                                                 (92,100,207),
                                                                 (93,101,208),
                                                                 (94,101,208),
                                                                 (95,102,209),
                                                                 (96,102,209);

INSERT INTO public.users_liked_posts(id,user_id,post_id) VALUES

                                                        (87,100,101),
                                                        (88,100,102),
                                                        (89,101,102),
                                                        (90,101,103),
                                                        (91,102,103),
                                                        (92,102,104);
INSERT INTO public.users_reposts (id,user_id,post_id) VALUES

                                                        (70,100,101),
                                                        (71,100,102),
                                                        (72,101,102),
                                                        (73,100,103),
                                                        (74,102,103),
                                                        (75,101,103);
