package com.example.bootdata.domain.hr;


import com.example.bootdata.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Setter
@Getter
@Entity
@Table(name = "reposts")
public class Repost extends BaseEntity {
    @ManyToOne(cascade ={CascadeType.MERGE } ,fetch = FetchType.EAGER )
    @JoinColumn(name = "post_id")
    private Post post;
  /*  @ManyToOne(cascade ={CascadeType.MERGE } ,fetch = FetchType.EAGER )
    @JoinColumn(name = "user_id")
    private User user;*/
}
