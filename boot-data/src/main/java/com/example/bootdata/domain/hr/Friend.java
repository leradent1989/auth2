package com.example.bootdata.domain.hr;


import com.example.bootdata.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import com.example.bootdata.domain.hr.User;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Setter
@Getter
@Entity

@Table(name = "friends")
@ToString
public class Friend extends BaseEntity {
 /* @OneToOne(cascade ={CascadeType.MERGE } ,fetch = FetchType.EAGER)
 @LazyCollection(LazyCollectionOption.FALSE)
 //@JsonIgnore
    @JoinColumn(name = "user_id", referencedColumnName = "entity_id")
private User user;
  @ManyToMany(cascade ={CascadeType.MERGE } ,fetch = FetchType.EAGER,mappedBy = "friends")
     @LazyCollection(LazyCollectionOption.FALSE)
     @JsonIgnore
    private List< User> friends = new ArrayList<>();

   */

    private String status;
 @ManyToOne(cascade ={CascadeType.MERGE } ,fetch = FetchType.EAGER )
 @JoinColumn(name = "user_id")
@JsonIgnore
    private User user;

    @ManyToOne(cascade ={CascadeType.MERGE } ,fetch = FetchType.EAGER )
    @JoinColumn(name = "friend_id")
    @JsonIgnore
  private  User friend;


}
