package com.example.bootdata.domain.hr;

import com.example.bootdata.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.*;
import com.example.bootdata.domain.hr.Repost;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.*;


@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "posts")
public class Post extends BaseEntity {

 private String content;

@JsonIgnore
 @ManyToOne(cascade = {CascadeType.PERSIST} ,fetch = FetchType.EAGER )
 @JoinColumn(name = "user_id")
 private User user;
@Column(name="post_type")
private String postType;
@Column(name="parent_id")
private Long parentId;


//@OneToMany(cascade = {CascadeType.MERGE,CascadeType.REMOVE },fetch = FetchType.EAGER ,mappedBy = "post")

// private List <Like> likes = new ArrayList<>();

@OneToMany(cascade = {CascadeType.MERGE,CascadeType.REMOVE },fetch = FetchType.EAGER )
@LazyCollection(LazyCollectionOption.FALSE)
@JsonIgnore
@JoinTable(
        name = "posts",
        joinColumns = { @JoinColumn(name = "parent_id") },
        inverseJoinColumns = { @JoinColumn(name = "entity_id") })
 private List <Post> comments = new ArrayList<>();


//@OneToMany(cascade = {CascadeType.MERGE,CascadeType.REMOVE },fetch = FetchType.EAGER ,mappedBy = "post")
//@LazyCollection(LazyCollectionOption.FALSE)
//@JsonIgnore
// private List <Repost> reposts = new ArrayList<>();



 @ManyToMany(mappedBy ="likedPosts",fetch = FetchType.EAGER)

 @JsonIgnore
 private List<User> likes;

 @ManyToMany(mappedBy ="reposts",fetch = FetchType.EAGER)
 @JsonIgnore
 private  List<User> reposts;

}


