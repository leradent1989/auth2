package com.example.bootdata.domain.hr;


import com.example.bootdata.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Setter
@Getter
@Entity
@Table(name = "post_images")
public class PostImage extends BaseEntity {
    @ManyToOne
    private Post post;

    @Column(name = "image_url")
    private String imageUrl ;
}
