package com.example.bootdata.domain.dto;

import jakarta.validation.constraints.*;
import lombok.*;
import org.hibernate.validator.constraints.Range;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class PostRequestDto {
    @NotNull
    @Min(1)
    @Max(1000)
    private Long id;
    @NotNull
    @Size(min = 2, message = "user name should have at least 2 characters")
    private String name;

    @NotNull
    @Size(min = 2, message = "user name should have at least 2 characters")
    private String imgUrl;
    @NotNull
    @Size(min = 2, message = "user name should have at least 2 characters")
    private String text;
}
