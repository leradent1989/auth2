package com.example.bootdata.domain.dto;

import com.example.bootdata.domain.hr.User;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class FriendRequestDto {
    private Long id;
    private User user;

    private String status;
}
