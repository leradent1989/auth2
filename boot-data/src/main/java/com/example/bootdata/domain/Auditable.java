package com.example.bootdata.domain;



import com.example.bootdata.domain.hr.User;
import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import java.lang.String;

import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Auditable<String> {
    @CreatedBy
    @Column(name = "created_by")
    private String  createdBy;

    @CreatedDate
    @Column(name = "creation_date")
    private Date creationDate;

    @LastModifiedBy
    @Column(name = "updated_by")
    private   String updatedBy;

    @LastModifiedDate
    @Column(name = "last_modified_date")
    private Date lastModifiedDate;
}
