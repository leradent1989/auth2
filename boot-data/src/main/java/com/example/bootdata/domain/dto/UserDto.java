package com.example.bootdata.domain.dto;

import com.example.bootdata.domain.hr.Chat;
import com.example.bootdata.domain.hr.Post;
import jakarta.persistence.Column;
import lombok.*;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class UserDto {
    private Long id;

    private String fullName;

    private String email;
    private String password;

    private Date birthDate;

    private String country;

    private String  city;
    private String  gender;

    private String workPlace;

    private String profilePicture;

    private String about;


    private String profileBackgroundPicture;

    private String createdBy;
   // private List<Chat> chats;

   // private List<Post> likedPosts;

 //   private List<Post> reposts;

}
