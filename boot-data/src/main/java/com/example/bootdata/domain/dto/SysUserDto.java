package com.example.bootdata.domain.dto;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class SysUserDto {

    private Long userId;

    private String userName;

    private String encryptedPassword;

    private boolean enabled;

    private String sysRoles;
}

