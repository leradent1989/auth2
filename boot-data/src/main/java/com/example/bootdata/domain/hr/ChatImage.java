package com.example.bootdata.domain.hr;


import com.example.bootdata.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Setter
@Getter
@Entity
@Table(name = "chat_images")
public class ChatImage extends BaseEntity {

    @ManyToOne(cascade ={CascadeType.MERGE } ,fetch = FetchType.EAGER )
    @JoinColumn(name = "chat_id")
    private Chat chat;

    @Column(name ="image_url")
    private  String imageUrl;
}
