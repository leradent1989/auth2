package com.example.bootdata.domain.dto;

import com.example.bootdata.domain.hr.User;
import jakarta.persistence.CascadeType;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.*;

import java.util.Date;
import java.util.UUID;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class MessageDto {
    private Long id;

    private String content;


    private User sender;
    protected Date lastModifiedDate;
}
