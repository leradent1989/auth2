package com.example.bootdata.domain.dto;

import com.example.bootdata.domain.hr.Like;
import com.example.bootdata.domain.hr.Post;
import com.example.bootdata.domain.hr.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class PostDto {

    private Long id;


    private String content;


  //  private User user;

    private String postType;

    private Long parentId;




   // private List<Like> likes = new ArrayList<>();


   // private List <Post> comments = new ArrayList<>();



    protected Date lastModifiedDate;
}
