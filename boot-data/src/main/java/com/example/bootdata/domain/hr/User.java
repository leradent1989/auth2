package com.example.bootdata.domain.hr;


import com.example.bootdata.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Getter
@Setter
@Entity
@JsonSerialize
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "users")
public class User extends BaseEntity  {


    @Column(name ="full_name")
   private String fullName;

   private String email;
   private String password;
   @Column(name = "birth_date")
   private Date birthDate;

   private String country;

   private String  city;
   private String  gender;
   @Column(name = "work_place")
   private String workPlace;
   @Column(name="profile_picture")
   private String profilePicture;

   private String about;
 /* @OneToOne(cascade ={CascadeType.MERGE } ,fetch = FetchType.EAGER,mappedBy = "user")
  @LazyCollection(LazyCollectionOption.FALSE)
  // @JsonIgnore
@JsonIgnore
  private Friend friend;*/
   @Column(name = "profile_background_picture")
   private String profileBackgroundPicture;
 /*  @LazyCollection(LazyCollectionOption.FALSE)
   @JsonIgnore
   @ManyToMany(cascade = { CascadeType.REFRESH },fetch = FetchType.EAGER)
  @JoinTable(
          name = "users_friends",
           joinColumns = { @JoinColumn(name = "user_id") },
           inverseJoinColumns = { @JoinColumn(name = "user_friend_id") })
       //   @OneToMany (cascade = {CascadeType.MERGE,CascadeType.REMOVE },fetch = FetchType.EAGER ,mappedBy = "user")
   private List<Friend> friends =new ArrayList<>();*/
 @OneToMany (cascade = {CascadeType.MERGE,CascadeType.REMOVE },fetch = FetchType.EAGER ,mappedBy = "user")
 @LazyCollection(LazyCollectionOption.FALSE)
         @JsonIgnore
    List <Friend> users;

    @OneToMany (cascade = {CascadeType.MERGE,CascadeType.REMOVE },fetch = FetchType.EAGER ,mappedBy = "friend")
    @LazyCollection(LazyCollectionOption.FALSE)
            @JsonIgnore
    List <Friend> friends;
   @OneToMany (cascade = {CascadeType.MERGE,CascadeType.REMOVE },fetch = FetchType.EAGER ,mappedBy = "user")
   @JsonIgnore
   private List<Post> posts  = new ArrayList<>();
   @OneToMany (cascade = {CascadeType.MERGE,CascadeType.REMOVE },fetch = FetchType.EAGER ,mappedBy = "sender")
   @LazyCollection(LazyCollectionOption.FALSE)
   @JsonIgnore
   private List<Message> messages=new ArrayList<>();


  /*@LazyCollection(LazyCollectionOption.FALSE)
   @ManyToMany(mappedBy = "participants",cascade = {CascadeType.REMOVE},fetch = FetchType.EAGER)
  @JsonIgnore*/
  @ManyToMany(cascade = { CascadeType.MERGE },fetch =FetchType.EAGER )
  @JsonIgnore
  @JoinTable(
          name = "users_chats",
          joinColumns = { @JoinColumn(name = "user_id") },
          inverseJoinColumns = { @JoinColumn(name = "chat_id") })
private List <Chat> chats=new ArrayList<>();
    @ManyToMany(cascade = { CascadeType.MERGE },fetch = FetchType.EAGER )
    @JsonIgnore
    @JoinTable(
            name = "users_liked_posts",
            joinColumns = { @JoinColumn(name = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "post_id") })
    private List<Post> likedPosts;
    @ManyToMany(cascade = { CascadeType.MERGE },fetch = FetchType.EAGER )
   @JsonIgnore
    @JoinTable(
            name = "users_reposts",
            joinColumns = { @JoinColumn(name = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "post_id") })
    private List<Post> reposts;


   @Override
   public String toString() {
      return "User{" +
              "fullName='" + fullName + '\'' +
              ", email='" + email + '\'' +
              ", password='" + password + '\'' +
              ", birthDate=" + birthDate +
              ", country='" + country + '\'' +
              ", city='" + city + '\'' +
              ", gender='" + gender + '\'' +
              ", workPlace='" + workPlace + '\'' +
              ", profilePicture='" + profilePicture + '\'' +
              ", about='" + about + '\'' +
              ", profileBackgroundPicture='" + profileBackgroundPicture + '\'' +
              '}';
   }
}

