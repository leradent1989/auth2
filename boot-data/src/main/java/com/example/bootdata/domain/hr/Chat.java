package com.example.bootdata.domain.hr;


import com.example.bootdata.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Setter
@Getter
@Entity
@Table(name = "chats")
public class Chat extends BaseEntity {
/* @ManyToMany(cascade = { CascadeType.MERGE },fetch =FetchType.EAGER )
 @JsonIgnore
    @JoinTable(
            name = "users_chats",
            joinColumns = { @JoinColumn(name = "user_id") },
            inverseJoinColumns = { @JoinColumn(name = "chat_id") })*/
@LazyCollection(LazyCollectionOption.FALSE)
@ManyToMany(mappedBy = "chats",cascade = {CascadeType.REMOVE},fetch = FetchType.EAGER)
@JsonIgnore
    List<User> participants;
    @OneToMany (cascade = {CascadeType.MERGE,CascadeType.REMOVE },fetch =FetchType.EAGER ,mappedBy = "chat")
    List <Message> messages;
    @OneToMany (cascade = {CascadeType.MERGE,CascadeType.REMOVE },fetch =FetchType.EAGER ,mappedBy = "chat")
    List <ChatImage> chatImages = new ArrayList<>();

}
