package com.example.bootdata.dao;

import com.example.bootdata.domain.hr.Post;
import com.example.bootdata.domain.hr.Repost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface RepostJpaRepository extends JpaRepository<Repost, Long>, JpaSpecificationExecutor<Repost> {



}
