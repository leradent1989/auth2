package com.example.bootdata.dao;


import com.example.bootdata.domain.BaseEntity;
import com.example.bootdata.domain.hr.User;

import com.example.bootdata.repository.RepositoryInterface;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;


public interface UserJpaRepository extends RepositoryInterface<User>, JpaSpecificationExecutor<User> {

    public Optional<User> getByEmail(@NonNull String email) ;


}
