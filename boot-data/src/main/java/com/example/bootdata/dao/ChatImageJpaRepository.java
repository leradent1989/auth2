package com.example.bootdata.dao;

import com.example.bootdata.domain.hr.ChatImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface ChatImageJpaRepository extends JpaRepository<ChatImage, Long>, JpaSpecificationExecutor<ChatImage> {



}
