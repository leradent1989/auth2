package com.example.bootdata.dao;

import com.example.bootdata.domain.hr.Chat;
import com.example.bootdata.domain.hr.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface ChatJpaRepository extends JpaRepository<Chat, Long>, JpaSpecificationExecutor<Chat> {



}
