package com.example.bootdata.dao;

import com.example.bootdata.domain.hr.Like;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface LikeJpaRepository extends JpaRepository<Like, Long>, JpaSpecificationExecutor<Like> {



}
