package com.example.bootdata.dao;

import com.example.bootdata.domain.hr.Friend;
import com.example.bootdata.domain.hr.Post;
import com.example.bootdata.domain.hr.User;
import com.example.bootdata.repository.RepositoryInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface FriendJpaRepository extends RepositoryInterface<Friend>, JpaSpecificationExecutor<Friend> {



}
