package com.example.bootdata.controller;

import com.example.bootdata.dao.UserJpaRepository;
import com.example.bootdata.domain.hr.User;
import com.example.bootdata.jwt.JwtRequest;
import com.example.bootdata.jwt.JwtResponse;
import com.example.bootdata.service.jwt.AuthService;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashSet;
import java.util.Locale;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
@Slf4j
@Validated
class Oauth2Controller {

    private final AuthService authService;

    private final UserJpaRepository repository;




    @GetMapping("oauth2/search")
    Optional<User> search(@RequestParam String email, OAuth2Authentication authentication) {
        //String auth = (String) authentication.;
        String role = authentication.getAuthorities().iterator().next().getAuthority();

        return repository.getByEmail(email);
    }

    @GetMapping("/oauth2/authorization/google")
  public ResponseEntity <?>  login( ) throws IOException {
        //
        //Authentication authentication1 = SecurityContextHolder.getContext();
    //OidcUser oauthUser = (OidcUser) authentication.getPrincipal();
       // String auth = (String)  oauthUser.getClaims().get("email");
        String auth = SecurityContextHolder.getContext().getAuthentication() .getPrincipal().toString();
        System.out.println(auth);

      //  String role = authentication.getAuthorities().iterator().next().getAuthority();
//if(auth.equals("anonymousUser")){
 //   return ResponseEntity.badRequest().body("Please sign into Google accounnt");
//}
//else {

    String access = authService.getRefreshStorage().get("token");
    String refresh = authService.getRefreshStorage().get("refresh");
    final JwtResponse token =new JwtResponse(access,refresh);
            //authService.loginAuth2(auth);
    return ResponseEntity.ok(token);
//}

    }


}
