package com.example.bootdata.resource;

import com.example.bootdata.domain.dto.MessageDto;
import com.example.bootdata.domain.dto.MessageRequestDto;
import com.example.bootdata.domain.hr.Message;
import com.example.bootdata.service.interfaces.MessageService;
import com.example.bootdata.service.dtomapper.MessageDtoMapper;
import com.example.bootdata.service.dtomapper.MessageRequestDtoMapper;
import com.example.bootdata.service.dtomapper.UserDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/messages")
@CrossOrigin(origins = {"http://localhost:3000"})
public class MessageRestController {

    private final MessageService messageService;

    private final MessageDtoMapper dtoMapper;
    private final UserDtoMapper userMapper;

    private final MessageRequestDtoMapper requestDtoMapper;
    @GetMapping

    public List<MessageDto> findAll(){
        return messageService.findAll(0,10).stream()
                .map(dtoMapper::convertToDto)
                .collect(Collectors.toList());

    }
    @GetMapping("/{page}/{size}")
    public ResponseEntity<?> findAll(@PathVariable Integer page, @PathVariable Integer size) {
        List<Message> messages = messageService.findAll(page, size);
        List<MessageDto>  messageDtoList = messages.stream().map(dtoMapper::convertToDto).collect(Collectors.toList());

        return ResponseEntity.ok(messageDtoList);
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable ("id") Long messageId){
         Message message = messageService.getById(messageId);
        if (message  == null){
            return ResponseEntity.badRequest().body("Message not found");
        }
        return ResponseEntity.ok().body(dtoMapper.convertToDto(message) );
    }
    @GetMapping("/{id}/sender")
    public ResponseEntity<?> getSender(@PathVariable ("id") Long messageId){
        Message message = messageService.getById(messageId);
        if (message  == null){
            return ResponseEntity.badRequest().body("Message not found");
        }
        return ResponseEntity.ok().build();
                //.body(userMapper.convertToDto(message) );
    }

//.getSender()
    @PostMapping
    public void create(@RequestBody MessageRequestDto message ){

        messageService.create(requestDtoMapper.convertToEntity( message));
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody MessageRequestDto messageRequestDto ){
        try {
            messageService.update(requestDtoMapper.convertToEntity(messageRequestDto)) ;
            return ResponseEntity.ok().build();
        } catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @DeleteMapping

    public ResponseEntity <?> deleteAccount(@RequestBody MessageRequestDto message){

        try{
           messageService.delete(requestDtoMapper.convertToEntity(message));
            return    ResponseEntity.ok().build();

        }catch (RuntimeException e){

            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable ("id") Long messageId){
        try {
            messageService.deleteById(messageId);
            return ResponseEntity.ok().build();
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }



}
