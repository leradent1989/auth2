package com.example.bootdata.resource;

import com.example.bootdata.domain.dto.UserDto;
import com.example.bootdata.domain.dto.UserRequestDto;


import com.example.bootdata.domain.hr.User;

import com.example.bootdata.service.GeneralService;
import com.example.bootdata.service.interfaces.UserService;

import com.example.bootdata.service.dtomapper.UserDtoMapper;
import com.example.bootdata.service.dtomapper.UserRequestDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserRestController {
    private final GeneralService <User> userService;

    private final UserDtoMapper dtoMapper;
    private final UserRequestDtoMapper requestMapper;

    @GetMapping
    public List<UserDto> getAll() {
        return userService.findAll().stream().map(dtoMapper::convertToDto).collect(Collectors.toList());
        //return userService.findAll(0,10);
    }
    @GetMapping("/{page}/{size}")
    public ResponseEntity<?> findAll(@PathVariable Integer page, @PathVariable Integer size) {
        List<User> users =userService.findAll();
        List<UserDto> userDtoList = users.stream().map(dtoMapper::convertToDto).collect(Collectors.toList());

        return ResponseEntity.ok(userDtoList);
    }
    @GetMapping("/{id}")
    public ResponseEntity<?>  getById(@PathVariable("id")  Long  userId) {
       User user = userService.getOne(userId );

        if (user   == null){
            return ResponseEntity.badRequest().body("Employer not found");
        }
        return ResponseEntity.ok().body(dtoMapper.convertToDto(user) );
    }
    @GetMapping("/{id}/friends")
    public ResponseEntity<?>  getFriends(@PathVariable("id")  Long  userId) {
        User user = userService.getOne(userId );

        if (user   == null){
            return ResponseEntity.badRequest().body("Employer not found");
        }
        return ResponseEntity.ok().body(user.getFriends() );
    }
    @GetMapping("/{id}/chats")
    public ResponseEntity<?>  getChats(@PathVariable("id")  Long  userId) {
        User user = userService.getOne(userId );

        if (user   == null){
            return ResponseEntity.badRequest().body("Employer not found");
        }
        return ResponseEntity.ok().body(user.getChats() );
    }
    @GetMapping("/{id}/posts")
    public ResponseEntity<?>  getPosts(@PathVariable("id")  Long  userId) {
        User user = userService.getOne(userId );

        if (user   == null){
            return ResponseEntity.badRequest().body("Employer not found");
        }
        return ResponseEntity.ok().body(user.getPosts() );
    }

    //.getFriends()
    @PostMapping
    public void create(@RequestBody UserRequestDto employee ){
        userService.save(requestMapper.convertToEntity(employee) );
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable("id")Long userId) {
        try {
         userService.deleteById(userId);
            return ResponseEntity.ok().build();
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @DeleteMapping
    public ResponseEntity<?> deleteEmployee(@RequestBody UserRequestDto company) {
        try {
            userService.delete(requestMapper.convertToEntity(company));
            return ResponseEntity.ok().build();
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @PutMapping
    public ResponseEntity <?> update(@RequestBody UserRequestDto employee) {
        try {
            userService.save(requestMapper.convertToEntity(employee));
            return ResponseEntity.ok().build();
        } catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }


}
