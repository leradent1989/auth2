package com.example.bootdata.resource;

import com.example.bootdata.domain.dto.FriendDto;
import com.example.bootdata.domain.dto.FriendRequestDto;



import com.example.bootdata.domain.hr.Friend;


import com.example.bootdata.domain.hr.User;
import com.example.bootdata.service.GeneralService;
import com.example.bootdata.service.dtomapper.FriendDtoMapper;
import com.example.bootdata.service.dtomapper.FriendRequestDtoMapper;
import com.example.bootdata.service.interfaces.FriendService;
import com.example.bootdata.service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/friends")
@Slf4j
public class FriendRestController {
   // private final FriendService friendService;
   private final GeneralService <Friend> friendService;

 //   private final UserService userService;

    private final FriendDtoMapper dtoMapper;
    private final FriendRequestDtoMapper requestMapper;

    @GetMapping
    public List<FriendDto> getAll() {
          return friendService.findAll().stream().map(dtoMapper::convertToDto).collect(Collectors.toList());
       // return friendService.findAll();
    }
    @GetMapping("/{page}/{size}")
    public ResponseEntity<?> findAll(@PathVariable Integer page, @PathVariable Integer size) {
        List<Friend> friends =friendService.findAll();
        List<FriendDto> userDtoList = friends.stream().map(dtoMapper::convertToDto).collect(Collectors.toList());

        return ResponseEntity.ok(userDtoList);
    }
   @GetMapping("/{id}")
    public ResponseEntity<?>  getById(@PathVariable("id")  Long  userId) {
        Friend friend = friendService.getOne(userId );

        if (friend   == null){
            return ResponseEntity.badRequest().body("Employer not found");
        }
        return ResponseEntity.ok().body(dtoMapper.convertToDto(friend) );
    }
    @GetMapping("/{userId}/friends")
    public ResponseEntity<?> getFriends(@PathVariable("userId")  Long  userId) {



        List<Friend> friends = friendService.findAll().stream().filter(el -> el.getUser().getId() == userId)
                .collect(Collectors.toList());

        List<FriendDto> usersFriends = friends.stream().map(dtoMapper::convertToDto).collect(Collectors.toList());

        return ResponseEntity.ok().body(usersFriends);
    }

    @PostMapping
    public void create(@RequestBody FriendRequestDto friend ){
        friendService.save(requestMapper.convertToEntity(friend) );
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable("id")Long userId) {
        try {
            friendService.deleteById(userId);
            return ResponseEntity.ok().build();
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @DeleteMapping
    public ResponseEntity<?> deleteEmployee(@RequestBody FriendRequestDto friend) {
        try {
            friendService.delete(requestMapper.convertToEntity(friend));
            return ResponseEntity.ok().build();
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @PutMapping
    public ResponseEntity <?> update(@RequestBody FriendRequestDto friend) {
        try {
            friendService.save(requestMapper.convertToEntity(friend));
            return ResponseEntity.ok().build();
        } catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }


}
