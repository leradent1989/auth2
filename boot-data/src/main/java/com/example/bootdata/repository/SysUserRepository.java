package com.example.bootdata.repository;

import com.example.bootdata.domain.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SysUserRepository extends JpaRepository<SysUser, Long> {
    Optional<SysUser> findUsersByUserName(String userName);

}
