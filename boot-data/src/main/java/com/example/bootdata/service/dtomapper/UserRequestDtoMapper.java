package com.example.bootdata.service.dtomapper;

import com.example.bootdata.domain.dto.UserRequestDto;
import com.example.bootdata.domain.hr.User;
import org.springframework.stereotype.Service;


@Service
public class UserRequestDtoMapper extends DtoMapperFacade<User, UserRequestDto>{
    public UserRequestDtoMapper (){super(User.class , UserRequestDto.class); }
}