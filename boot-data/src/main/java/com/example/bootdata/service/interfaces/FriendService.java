package com.example.bootdata.service.interfaces;

import com.example.bootdata.domain.hr.Friend;

import java.util.List;
import java.util.UUID;

public interface  FriendService {
    List<Friend> findAll(Integer page, Integer size);
    void save(Friend friend );
    void update(Friend friend );
    void delete(Friend friend );
    void deleteAll(List<Friend> friendList );
    void saveAll (List <Friend> friendList );
    List<Friend> findAll();
    void deleteById(Long id);
    Friend getOne(long id);


}
