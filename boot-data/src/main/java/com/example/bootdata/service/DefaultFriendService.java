package com.example.bootdata.service;

import com.example.bootdata.dao.FriendJpaRepository;
import com.example.bootdata.dao.UserJpaRepository;
import com.example.bootdata.domain.hr.Friend;
import com.example.bootdata.service.interfaces.FriendService;
import com.example.bootdata.service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;


import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class DefaultFriendService extends GeneralService<Friend> {

    private final FriendJpaRepository friendRepository;


}
