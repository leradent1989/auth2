package com.example.bootdata.service.dtomapper;

import com.example.bootdata.domain.dto.PostDto;

import com.example.bootdata.domain.dto.PostRequestDto;
import com.example.bootdata.domain.hr.Post;

import org.springframework.stereotype.Service;

@Service
public class PostDtoMapper extends GeneralFacade<Post,PostRequestDto ,PostDto >{
    //public PostDtoMapper(){super(Post.class , PostDto.class); }
}
