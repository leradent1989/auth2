package com.example.bootdata.service.interfaces;


import com.example.bootdata.domain.hr.Message;

import java.util.List;

public interface MessageService {
    List<Message> findAll(Integer page, Integer size);
    List<Message> getAll();
     void create(Message employee);
    Message getById(Long userId);
    void deleteById(Long id );
    void update(Message employee);
    void delete(Message employee );
}
