package com.example.bootdata.service.interfaces;

import com.example.bootdata.domain.hr.User;

import java.util.List;
import java.util.UUID;

public interface  UserService {
  List<User> findAll(Integer page, Integer size);
    void save(User account );
  void update(User account );
    void delete(User account );
    void deleteAll(List<User> accountList );
    void saveAll (List <User> accountList );
    List<User> findAll();
  void deleteById(Long id);
    User getOne(long id);


}
