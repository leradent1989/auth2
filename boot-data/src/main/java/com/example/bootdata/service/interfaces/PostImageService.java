package com.example.bootdata.service.interfaces;

import com.example.bootdata.domain.hr.PostImage;

import java.util.List;
import java.util.UUID;

public interface  PostImageService {
    List<PostImage> findAll(Integer page, Integer size);
    void save(PostImage postImage );
    void update(PostImage postImage );
    void delete(PostImage postImage );
    void deleteAll(List<PostImage> postImageList );
    void saveAll (List <PostImage> accountList );
    List<PostImage> findAll();
    void deleteById(Long id);
    PostImage getOne(long id);


}
