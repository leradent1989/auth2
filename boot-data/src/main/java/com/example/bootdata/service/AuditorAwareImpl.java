package com.example.bootdata.service;

import com.example.bootdata.domain.hr.User;
import com.example.bootdata.jwt.JwtAuthentication;
import com.example.bootdata.service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
@Service
@RequiredArgsConstructor
@Transactional
public class AuditorAwareImpl implements AuditorAware<String>{


    private final JwtAuthentication jwtAuthentication;
    public Optional< String > getCurrentAuditor() {


            return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());

        }



        }

