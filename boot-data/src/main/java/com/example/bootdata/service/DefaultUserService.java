package com.example.bootdata.service;


import com.example.bootdata.dao.UserJpaRepository;
import com.example.bootdata.domain.hr.User;
import com.example.bootdata.service.interfaces.UserService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class DefaultUserService extends GeneralService <User> {

    private final UserJpaRepository userRepository;



    public Optional<User> getByEmail(@NonNull String email){
        return userRepository.getByEmail(email);
    }



}
