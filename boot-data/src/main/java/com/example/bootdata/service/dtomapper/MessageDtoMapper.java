package com.example.bootdata.service.dtomapper;


import com.example.bootdata.domain.dto.MessageDto;
import com.example.bootdata.domain.hr.Message;
import org.springframework.stereotype.Service;


@Service
public class MessageDtoMapper extends DtoMapperFacade<Message, MessageDto>{
    public MessageDtoMapper(){super(Message.class , MessageDto.class); }
}
