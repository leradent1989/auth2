package com.example.bootdata.service.dtomapper;

import com.example.bootdata.domain.dto.FriendDto;
import com.example.bootdata.domain.dto.FriendRequestDto;
import com.example.bootdata.domain.dto.UserDto;
import com.example.bootdata.domain.hr.Friend;
import com.example.bootdata.domain.hr.User;
import org.springframework.stereotype.Service;


@Service
public class FriendDtoMapper extends GeneralFacade<Friend, FriendRequestDto,FriendDto>{
   // public FriendDtoMapper (){super(Friend.class , FriendDto.class); }
}

