package com.example.bootdata.service;



import com.example.bootdata.dao.MessageJpaRepository;
import com.example.bootdata.domain.hr.Message;

import com.example.bootdata.service.interfaces.MessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@Transactional
@RequiredArgsConstructor
public class DefaultMessageService implements MessageService {
    private final MessageJpaRepository messageRepository;

    public List<Message> findAll(Integer  page, Integer size){

        Sort sort =  Sort.by(new Sort.Order(Sort.Direction.ASC,"id"));
        Pageable pageable = PageRequest.of(page,size,sort);
        Page<Message> departmentPage = messageRepository.findAll(pageable);

        return departmentPage.toList();

    }


    @Override
    @Transactional(readOnly = true, rollbackFor = NoSuchElementException.class, timeout = 1000)
    public List<Message> getAll() {
        return messageRepository.findAll();
    }
    @Override
    public void create(Message employee) {
        messageRepository.save(employee);
    }

    @Override
    @Transactional(readOnly = true)
    public Message getById(Long userId) {
        return messageRepository.getById(userId);
    }

    @Override
    public void update(Message employee) {
        employee.setCreationDate(messageRepository.getById(employee.getId()).getCreationDate());
        messageRepository.save(employee);

    }

    public void deleteById(Long id) {
        messageRepository.deleteById(id);

    }
    @Override
    public void delete(Message employee) {
        messageRepository.delete(employee);

    }

}
