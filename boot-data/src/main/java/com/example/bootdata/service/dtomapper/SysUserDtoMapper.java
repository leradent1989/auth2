package com.example.bootdata.service.dtomapper;



import com.example.bootdata.domain.SysRole;
import com.example.bootdata.domain.SysUser;
import com.example.bootdata.domain.dto.SysUserDto;
import com.example.bootdata.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class SysUserDtoMapper extends DtoMapperFacade<SysUser, SysUserDto> {
    public SysUserDtoMapper() {
        super(SysUser.class, SysUserDto.class);
    }

    @Override
    protected void decorateDto(SysUserDto dto, SysUser entity) {
        String roles = entity.getSysRoles().stream().map(SysRole::getRoleName).collect(Collectors.joining(", "));
        dto.setSysRoles(roles);
    }
}
