package com.example.bootdata.service.jwt;


import com.example.bootdata.dao.UserJpaRepository;
import com.example.bootdata.domain.SysUser;
import com.example.bootdata.domain.hr.User;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserJpaRepository userRepository;

    public Optional<User> getByEmail(@NonNull String email) {

        return userRepository.getByEmail(email);
    }

}