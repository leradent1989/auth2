package com.example.bootdata.service.dtomapper;

import com.example.bootdata.domain.dto.FriendDto;
import com.example.bootdata.domain.dto.FriendRequestDto;
import com.example.bootdata.domain.dto.UserDto;
import com.example.bootdata.domain.hr.Friend;
import com.example.bootdata.domain.hr.User;
import org.springframework.stereotype.Service;


@Service
public class FriendRequestDtoMapper extends DtoMapperFacade<Friend, FriendRequestDto>{
    public FriendRequestDtoMapper (){super(Friend.class , FriendRequestDto.class); }
}

