package com.example.bootdata.service;

import com.example.bootdata.dao.PostJpaRepository;

import com.example.bootdata.domain.hr.Post;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DefaultPostServiceTest {

    @Mock
    private PostJpaRepository postJpaRepository;


    @InjectMocks
    private DefaultPostService customerService;

    @Captor
    private ArgumentCaptor<Post> customerArgumentCaptor;

    @Test
    public void testGetAllPageble() {
       Post post= new Post();
        when(postJpaRepository.findAll(any(Pageable.class)))
                .thenReturn(new PageImpl<>(List.of(post)));
        List<Post> posts = customerService.findAll(1, 2);

        assertEquals(post, posts.get(0));
    }

    @Test
    public void test_GetAll_Success() {
      Post post1 = new Post();
        Post post2 = new Post();
        List<Post> customersExpected = List.of(post1, post2);
        when(postJpaRepository.findAll())
                .thenReturn(customersExpected);

        List<Post> customersActual =customerService.findAll();
        assertNotNull(customersActual);
        assertFalse(customersActual.isEmpty());
        assertIterableEquals(customersExpected, customersActual);
    }

    @Test
    public void test_Create_Success() {
        Post customer1 = new Post();

        customerService.save(customer1);

        verify(postJpaRepository).save(customerArgumentCaptor.capture());
        Post customerActualArgument = customerArgumentCaptor.getValue();
        assertEquals(customer1, customerActualArgument);
    }
    @Test
    public void test_Put_Success() {
        Post customer1 = new Post() ;



        customer1.setCreationDate(new Date());
        when(postJpaRepository.getOne(customer1.getId())
        )
                .thenReturn(customer1);
        customerService.update(customer1);

        verify(postJpaRepository).save(customerArgumentCaptor.capture());
      Post customerActualArgument = customerArgumentCaptor.getValue();
        assertEquals(customer1, customerActualArgument);
    }
    @Test
    public void test_Delete_Success() {
        Post customer1= new Post() ;

       customerService.save(customer1);
        customer1.setId(1L);


        customerService.delete(customer1);

        verify(postJpaRepository).delete(customerArgumentCaptor.capture());
        Post customerActualArgument = customerArgumentCaptor.getValue();
        assertEquals(customer1, customerActualArgument);
    }
    @Test
    public void test_GetById_Success() {
        Post customer1 = new Post();
        customer1.setId(1L);
        Post customer2 = new Post();
        customer2.setId(2L);
       Post customerExpected = customer2;
        when(postJpaRepository.getOne(customer2.getId()))
                .thenReturn(customer2);

        Post customerActual =customerService.getOne(customer2.getId());
        assertNotNull(customerActual);

        assertEquals(customerExpected, customerActual);
    }

}