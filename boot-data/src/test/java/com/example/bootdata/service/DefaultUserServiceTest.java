package com.example.bootdata.service;

import com.example.bootdata.dao.UserJpaRepository;

import com.example.bootdata.domain.hr.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DefaultUserServiceTest {

    @Mock
    private UserJpaRepository accountJpaRepository;


    @InjectMocks
    private DefaultUserService accountService;

    @Captor
    private ArgumentCaptor<User> accountArgumentCaptor;

    @Test
    public void testGetAllPageble() {
        User account = new User();
        when(accountJpaRepository.findAll(any(Pageable.class)))
                .thenReturn(new PageImpl<>(List.of(account)));
      //  List<User> accounts = accountService.findAll(1, 2);

       // assertEquals(account, accounts.get(0));
    }

    @Test
    public void test_GetAll_Success() {
        User account1 = new User();
        User account2 = new User();
        List<User> accountsExpected = List.of(account1, account2);
        when(accountJpaRepository.findAll())
                .thenReturn(  accountsExpected);

        List<User> accountsActual = accountService.findAll();
        assertNotNull(accountsActual);
        assertFalse(accountsActual.isEmpty());
        assertIterableEquals(accountsExpected, accountsActual);
    }

    @Test
    public void test_Create_Success() {
       User account1 = new User() ;

        accountService.save(account1);

        verify(accountJpaRepository).save(accountArgumentCaptor.capture());
        User accountActualArgument = accountArgumentCaptor.getValue();
        assertEquals(account1, accountActualArgument);
    }
    @Test
    public void test_Put_Success() {
        User account1 = new User() ;



        when(accountJpaRepository.getOne(account1.getId())
        )
                .thenReturn(account1);

       // accountService.update(account1);

        verify(accountJpaRepository).save(accountArgumentCaptor.capture());
        User accountActualArgument = accountArgumentCaptor.getValue();
        assertEquals(account1, accountActualArgument);
    }
    @Test
    public void test_Delete_Success() {
        User account1 = new User() ;

        accountService.save(account1);
        account1.setId(1L);


        accountService.delete(account1);

        verify(accountJpaRepository).delete(accountArgumentCaptor.capture());
        User accountActualArgument = accountArgumentCaptor.getValue();
        assertEquals(account1, accountActualArgument);
    }
    @Test
    public void test_GetById_Success() {
       User account1 = new User();
        account1.setId(1L);
        User account2 = new User();
        account2.setId(2L);
        User accountExpected = account2;
        when(accountJpaRepository.getOne(account2.getId()))
                .thenReturn(account2);

        User accountActual =accountService.getOne(account2.getId());
        assertNotNull(accountActual);

        assertEquals(accountExpected, accountActual);
    }
}