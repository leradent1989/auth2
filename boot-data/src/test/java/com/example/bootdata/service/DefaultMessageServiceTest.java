package com.example.bootdata.service;

import com.example.bootdata.dao.MessageJpaRepository;
import com.example.bootdata.domain.hr.Message;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DefaultMessageServiceTest {

    @Mock
    private MessageJpaRepository messageJpaRepository;


    @InjectMocks
    private DefaultMessageService messageService;

    @Captor
    private ArgumentCaptor<Message> employerArgumentCaptor;

    @Test
    public void testGetAllPageble() {
        Message employer = new Message();
        when(messageJpaRepository.findAll(any(Pageable.class)))
                .thenReturn(new PageImpl<>(List.of(employer)));
        List<Message> employers = messageService.findAll(1, 2);

        assertEquals(employer, employers.get(0));
    }

    @Test
    public void test_GetAll_Success() {
       Message employer1 = new Message();
       Message employer2 = new Message();
        List<Message> employersExpected = List.of(employer1, employer2);
        when(messageJpaRepository.findAll())
                .thenReturn(employersExpected);

        List<Message> employersActual =messageService.getAll();
        assertNotNull(employersActual);
        assertFalse(employersActual.isEmpty());
        assertIterableEquals(employersExpected, employersActual);
    }
    @Test
    public void test_GetById_Success() {
        Message employer1 = new Message();
        employer1.setId(1L);
        Message employer2 = new Message();
        employer2.setId(2L);
        Message employerExpected = employer1;
        when(messageJpaRepository.getById(employer2.getId()))
                .thenReturn(employer2);

        Message employerActual =messageService.getById(employer2.getId());
        assertNotNull(employerActual);

        assertEquals(employerExpected, employerActual);
    }

    @Test
    public void test_Create_Success() {
       Message employer1 = new Message();

       messageService.create(employer1);

        verify(messageJpaRepository).save(employerArgumentCaptor.capture());
        Message employerActualArgument = employerArgumentCaptor.getValue();
        assertEquals(employer1, employerActualArgument);
    }
    @Test
    public void test_Put_Success() {
        Message employer1= new Message() ;



        employer1.setCreationDate(new Date());
        when(messageJpaRepository.getById(employer1.getId())
        )
                .thenReturn(employer1);
        messageService.update(employer1);

        verify(messageJpaRepository).save(employerArgumentCaptor.capture());
       Message employerActualArgument = employerArgumentCaptor.getValue();
        assertEquals(employer1, employerActualArgument);
    }
    @Test
    public void test_Delete_Success() {
        Message employer1= new Message() ;

        messageService.create(employer1);
       employer1.setId(1L);


        messageService.delete(employer1);

        verify(messageJpaRepository).delete(employerArgumentCaptor.capture());
        Message employerActualArgument = employerArgumentCaptor.getValue();
        assertEquals(employer1, employerActualArgument);
    }


}