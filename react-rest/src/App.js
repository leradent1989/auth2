import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import instance from './instance';
import Login from "./Login";
import Users from "./Users";

class App extends Component {
 /* constructor(props) {
    super(props);
    this.state = {message:[]};
  }

 async componentDidMount() {
    const token = await axios.get("http://localhost:9000/api/oauth2/authorization/google")
    localStorage.setItem('token',JSON.stringify(token))

    const url = 'http://localhost:9000/users';
   await  instance.get(url).then(response => {
      console.log('response : ');
      console.log(response);
      this.setState({message: response.data});
    }).catch(error => {
      console.log(error);
    });
  }*/

  render() {
    if(!localStorage.getItem('token')){
      return (

          <>

         <Login/>
          </>
      )
    }else{
    return (
        <div className="App">
        <Users/>
        </div>
    );
  }
  }
}

export default App;